package com.mphasis.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mphasis.LoginDAOs.LoginDAO;
import com.mphasis.beans.User;

@RestController
@RequestMapping("/")
public class LoginController {

	@Autowired
	LoginDAO login;

	@PostMapping(value = "register", consumes = MediaType.APPLICATION_JSON_VALUE)
	public String registerUser(@RequestBody User user) {
		System.out.println(user);
		if (user.getUsername().equals("")) {
			return "Please enter Username";

		} else if (user.getPassword().equals("")) {
			return "Please enter Password";

		} else {

			if (login.insertUser(user)) {
				return "Registered Successfully";
			} else {
				return "Username Already Exists";
			}

		}
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ResponseEntity<String> loginUser(@RequestBody User user) {

		if (user.getUsername().equals("")) {
			return new ResponseEntity<String>("Please enter Username", HttpStatus.CONFLICT);

		} else if (user.getPassword().equals("")) {
			return new ResponseEntity<String>("Please enter Password", HttpStatus.CONFLICT);
		} else {

			if (validateUser(user)) {
				return new ResponseEntity<String>("Welcome " + user.getUsername(), HttpStatus.OK);
			} else {
				return new ResponseEntity<String>("Incorrect Username Or Password", HttpStatus.UNAUTHORIZED);
			}

		}

	}

	/*
	 * @RequestMapping(value= {"/logout"}) public ModelAndView logout() { return new
	 * ModelAndView("index", "message", "Logged Out Successfully"); }
	 */

	@GetMapping("hello")
	public String homePage() {
		System.out.println("hello");
		return "hello and welcome";
	}

	public boolean validateUser(User user) {
		User retrieved = login.getUser(user.getUsername());

		if (retrieved == null) {
			return false;
		} else if (retrieved.getPassword().equals(user.getPassword())) {
			return true;
		} else {
			return false;
		}

	}

}
