package com.mphasis.LoginDAOs;

import com.mphasis.beans.User;

public interface LoginDAO {

	public boolean insertUser(User user);
	
	public User getUser(String username);
}
