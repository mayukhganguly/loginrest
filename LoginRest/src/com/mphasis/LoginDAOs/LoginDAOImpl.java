package com.mphasis.LoginDAOs;

import java.util.List;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import com.mphasis.beans.User;

public class LoginDAOImpl implements LoginDAO {

	private JdbcTemplate jdbcTemplate;
	
	@Override
	public boolean insertUser(User user) {
		
		if(getUser(user.getUsername())==null)
		{
			String sql="insert into login values(?,?)";
			jdbcTemplate.update(sql, user.getUsername(),user.getPassword());
			return true;
		}
		return false;
	}

	@Override
	public User getUser(String username) {
		
		String sql="select username, password from login where username=?";
		
		List<User> users=jdbcTemplate.query(sql,new Object[] {username},new BeanPropertyRowMapper<>(User.class));
		
		if(users.isEmpty())
		{
			return null;
		}
		else {
			return users.get(0);
		}
		
		
	}

	 public void setJdbcTemplate(JdbcTemplate jdbcTemplate){
	        this.jdbcTemplate = jdbcTemplate;
	       
	    }
}
