package com.mphasis.Confuigerations;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.mphasis.LoginDAOs.LoginDAO;
import com.mphasis.LoginDAOs.LoginDAOImpl;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "com")
public class LoginJdbcConfiguration implements WebMvcConfigurer {

	/*
	 * @Override public void
	 * configureMessageConverters(List<HttpMessageConverter<?>> converters) {
	 * Jackson2ObjectMapperBuilder builder=new Jackson2ObjectMapperBuilder();
	 * builder.indentOutput(true); converters.add(new
	 * MappingJackson2CborHttpMessageConverter(builder.build()));
	 * 
	 * }
	 */

	@Bean
	public DataSource dataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();

		dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		dataSource.setUrl("jdbc:mysql://localhost:3306/practicedb");
		dataSource.setUsername("mayukh");
		dataSource.setPassword("tatai100");

		return dataSource;
	}

	@Bean
	public JdbcTemplate jdbcTemplate() {
		JdbcTemplate jdbcTemplate = new JdbcTemplate();
		jdbcTemplate.setDataSource(dataSource());
		return jdbcTemplate;
	}

	@Bean
	public LoginDAO loginDAO() {
		LoginDAOImpl logDao = new LoginDAOImpl();
		logDao.setJdbcTemplate(jdbcTemplate());
		return logDao;
	}

}
